const webdriver = require('selenium-webdriver'),
    {describe, it, after, before, beforeEach, afterEach} = require('selenium-webdriver/testing'),
    By = webdriver.By,
    assert = require('assert'),
    until = webdriver.until;
const mlog = require('mocha-logger');
let find;

describe('moveexpert scenario', function () {
    let driver;
    beforeEach(function () {
        driver = new webdriver.Builder().forBrowser('chrome').build();
        driver.manage().window().maximize();
        driver.get('https://13:13@moveexpert.stg.tekoway.com');

    });

    afterEach(function () {
        driver.quit();
    });

    it('simple test', function () {

        driver.wait(
            until.elementLocated(By.xpath('//*[@id="gdpr-banner"]/div[2]/div/button[2]')),
            30000
        );
        driver.findElement(By.xpath('//*[@id="gdpr-banner"]/div[2]/div/button[2]'))
            .click();
        driver.findElement(By.linkText('Companies')).click();
    });
});
